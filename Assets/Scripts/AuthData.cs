﻿using System;
using Ilych.Cyberpunk0.Model;
using UnityEngine;

namespace Ilych.Cyberpunk0
{
	[CreateAssetMenu]
	public class AuthData : ScriptableObject
	{
		public UserId? user { get; private set; }

		public bool? isAdmin { get; private set; }

		public AuthToken? authToken { get; private set; }

		public event Action Changed;

		public void UpdateWithLoginInformation(UserId user, bool isAdmin, AuthToken authToken)
		{
			this.user = user;
			this.isAdmin = isAdmin;
			this.authToken = authToken;

			InvokeChangedEvent();
		}

		public void FillWithDummy()
		{
			UpdateWithLoginInformation(
				user: new UserId(2),
				isAdmin: true,
				authToken: new AuthToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoyLCJwZXJtaXNzaW9ucyI6M30.GDCHmr99Tf7hoBiW8tkV8lVgnseP0ZgcQZV2jDe1Bac"));
		}

		public void Reset()
		{
			user = null;
			isAdmin = null;
			authToken = null;

			InvokeChangedEvent();
		}

		private void OnEnable()
		{
			Reset();
		}

		private void InvokeChangedEvent()
		{
			var ev = Changed;
			if (ev != null)
			{
				ev.Invoke();
			}
		}
	}
}
