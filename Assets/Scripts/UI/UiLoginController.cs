﻿using System.Collections;
using System.Collections.Generic;
using Ilych.Cyberpunk0;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class UiLoginController : MonoBehaviour
{
	[SerializeField] private LoginController controller;
	[SerializeField] private InputField login;
	[SerializeField] private InputField password;

	[PublicAPI]
	public void StartSignIn()
	{
		controller.StartSignIn(
			login: login.text,
			password: password.text);
	}

	[PublicAPI]
	public void SignOut()
	{
		controller.SignOut();
	}
}
