﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Ilych.Cyberpunk0.UI
{
  public class UiAuthStatusController : MonoBehaviour
  {
    public AuthData authData;
    public UnityEvent onLoggedIn;
    public UnityEvent onLoggedOut;

    private bool isLoggedIn;

    private void OnEnable()
    {
      authData.Changed += OnAuthDataChanged;
      var isLoggedIn = GetIsLoggedIn();
      this.isLoggedIn = isLoggedIn;
      (isLoggedIn ? onLoggedIn : onLoggedOut).Invoke();
    }

    private void OnDisable()
    {
      authData.Changed -= OnAuthDataChanged;
    }

    private void OnAuthDataChanged()
    {
      var isLoggedIn = GetIsLoggedIn();
      if (isLoggedIn != this.isLoggedIn)
      {
        this.isLoggedIn = isLoggedIn;
        (isLoggedIn ? onLoggedIn : onLoggedOut).Invoke();
      }
    }

    private bool GetIsLoggedIn()
    {
      return authData.user != null;
    }
  }
}
