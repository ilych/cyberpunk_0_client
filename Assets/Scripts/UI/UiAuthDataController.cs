﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Ilych.Cyberpunk0.UI
{
  public class UiAuthDataController : MonoBehaviour
  {
    public AuthData authData;
    public InputField labelUserId;
    public Toggle toggleIsAdmin;
    public InputField labelToken;

    private void OnEnable()
    {
      authData.Changed += OnAuthDataChanged;
      Validate();
    }

    private void OnDisable()
    {
      authData.Changed -= OnAuthDataChanged;
    }

    private void OnAuthDataChanged()
    {
      Validate();
    }

    private void Validate()
    {
      labelUserId.text = authData.user.HasValue ? authData.user.Value.raw.ToString() : "<empty>";
		  toggleIsAdmin.isOn = authData.isAdmin ?? false;
      labelToken.text = authData.authToken.HasValue ? authData.authToken.Value.raw : "<empty>";
    }
  }
}
