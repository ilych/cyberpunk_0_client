﻿using UnityEngine;
using UnityEngine.UI;

namespace Ilych.Cyberpunk0.UI
{
  public class UiControllerLoginInfo : MonoBehaviour
  {
    public AuthData authData;
    public Text labelUserId;
    public Toggle toggleIsAdmin;
    public Text labelToken;

    private void OnDisable()
    {
      authData.Changed -= OnAuthDataChanged;
    }

    private void OnEnable()
    {
      authData.Changed += OnAuthDataChanged;
      Validate();
    }

    private void OnAuthDataChanged()
    {
      Validate();
    }

    private void Validate()
    {
      labelUserId.text = authData.user.HasValue ? authData.user.Value.raw.ToString() : "<empty>";
		  toggleIsAdmin.isOn = authData.isAdmin ?? false;
      labelToken.text = authData.authToken.HasValue ? authData.authToken.Value.raw : "<empty>";
    }
  }
}
