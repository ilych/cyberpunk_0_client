﻿using System.Text;
using Ilych.Cyberpunk0.Model;

namespace Ilych.Cyberpunk0.ServerAPI
{
  public struct APIMethod
  {
    public static APIMethod login()
    {
      return new APIMethod {
        path = "auth/token"
      };
    }

    public static APIMethod profile(UserId user)
    {
      var builder = new StringBuilder("users/");
      builder.Append(user.raw);
      return new APIMethod {
        path = builder.ToString()
      };
    }

    internal string path { get; private set; }
  }
}
