﻿using System.IO;
using System.Text;
using Ilych.Cyberpunk0.Model;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;

namespace Ilych.Cyberpunk0.ServerAPI
{
  [CreateAssetMenu]
  public class ServerAPIConfig : ScriptableObject
  {
    public ServerInfo server;
    public string versionSuffix;

    public UnityWebRequest prepareWebRequest(APIMethod method, [CanBeNull] object payload)
    {
      var body = payload != null ? JsonUtility.ToJson(payload) : null;
      var bytes = body != null ? Encoding.UTF8.GetBytes(body) : null;
      var url = makeRequestUrl(method);
      var request = new UnityWebRequest(
        url: url,
        method: bytes != null ? UnityWebRequest.kHttpVerbPOST : UnityWebRequest.kHttpVerbGET,
        downloadHandler: new DownloadHandlerBuffer(),
        uploadHandler: bytes != null ? new UploadHandlerRaw(bytes) : null);
      if (bytes != null)
      {
        request.SetRequestHeader("Content-Type", "application/json");
      }
      return request;
    }

    public UnityWebRequest prepareWebRequestWithAuthorization(APIMethod method, AuthToken authToken, [CanBeNull] object payload)
    {
      var request = prepareWebRequest(method, payload);
      request.SetRequestHeader("Authorization", authToken.raw);
      return request;
    }

    private string makeRequestUrl(APIMethod method)
    {
      var prefix = Path.Combine(server.baseUrl, versionSuffix);
      return Path.Combine(prefix, method.path);
    }
  }
}
