﻿using UnityEngine;

namespace Ilych.Cyberpunk0.ServerAPI
{
  [CreateAssetMenu]
  public class ServerInfo : ScriptableObject
  {
    public string baseUrl;
  }
}