﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilych.Cyberpunk0.Model;
using UnityEngine;
using Attribute = Ilych.Cyberpunk0.Model.Attribute;

namespace Ilych.Cyberpunk0
{
  [CreateAssetMenu]
  public class ProfileData : ScriptableObject
  {
    public int credits { get; private set; }
    public IEnumerable<Attribute> attributes { get; private set; }
    public IEnumerable<Ability> abilities { get; private set; }
    public IEnumerable<Item> items { get; private set; }

    public event Action Changed;

    public void UpdateWithProfileInformation(int credits, Attribute[] attributes, Ability[] nativeAbilities, Item[] items)
    {
      this.credits = credits;
      this.attributes = attributes;
      this.abilities = PrepareAbilities(nativeAbilities.Concat(items.SelectMany(i => i.providedAbilities)));
      this.items = items;

      InvokeChangedEvent();
    }

    public void Reset()
    {
      credits = 0;
      attributes = new Attribute[0];
      abilities = new Ability[0];
      items = new Item[0];

      InvokeChangedEvent();
    }

    private void OnEnable()
    {
      Reset();
    }

    private void InvokeChangedEvent()
    {
      var ev = Changed;
      if (ev != null)
      {
        ev();
      }
    }

    private Ability[] PrepareAbilities(IEnumerable<Ability> abilities)
    {
      var sorted = abilities
        .OrderBy(a => a.name)
        .ThenByDescending(a => a.isNative);

      Ability prev = null;
      var buffer = new List<Ability>();
      foreach (var a in sorted)
      {
        if (prev != null
            && a.name.Equals(prev.name, StringComparison.Ordinal)
            && a.isNative == prev.isNative)
        {
          continue;
        }

        prev = a;
        buffer.Add(a);
      }

      return buffer.ToArray();
    }
  }
}
