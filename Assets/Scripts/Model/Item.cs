﻿using System.Collections.Generic;

namespace Ilych.Cyberpunk0.Model
{
  public class Item
  {
    public string name { get; private set; }
    public IEnumerable<Ability> providedAbilities { get; private set; }

    public Item(string name, Ability[] providedAbilities)
    {
      this.name = name;
      this.providedAbilities = providedAbilities;
    }
  }
}
