﻿namespace Ilych.Cyberpunk0.Model
{
  public struct UserId
  {
    public long raw { get; private set; }

    public UserId(long raw)
    {
      this = new UserId {raw = raw};
    }
  }
}
