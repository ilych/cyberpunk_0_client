﻿namespace Ilych.Cyberpunk0.Model
{
  public class Ability
  {
    public string name { get; private set; }
    public bool isNative { get; private set; }

    public Ability(string name, bool isNative)
    {
      this.name = name;
      this.isNative = isNative;
    }
  }
}
