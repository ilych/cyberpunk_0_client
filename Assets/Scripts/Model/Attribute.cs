﻿namespace Ilych.Cyberpunk0.Model
{
  public class Attribute
  {
    public string name { get; private set; }
    public int value { get; private set; }

    public Attribute(string name, int value)
    {
      this.name = name;
      this.value = value;
    }
  }
}
