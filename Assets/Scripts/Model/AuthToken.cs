﻿namespace Ilych.Cyberpunk0.Model
{
  public struct AuthToken
  {
    public string raw { get; private set; }

    public AuthToken(string raw)
    {
      this = new AuthToken {raw = raw};
    }
  }
}
