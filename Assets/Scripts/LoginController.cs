﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Ilych.Cyberpunk0.Model;
using Ilych.Cyberpunk0.ServerAPI;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Networking;
using UnityEngine.UI;
using Attribute = Ilych.Cyberpunk0.Model.Attribute;

namespace Ilych.Cyberpunk0
{
    public class LoginController : MonoBehaviour
    {
        public ServerAPIConfig apiConfig;
        public AuthData authData;
        public ProfileData profileData;

        [PublicAPI]
        public void StartSignIn(string login, string password)
        {
            StartCoroutine(SignIn(
                login: login,
                password: password));
        }

        [PublicAPI]
        public void SignOut()
        {
            authData.Reset();
        }

        private IEnumerator SignIn(string login, string password)
        {
            var loginRequestData = new LoginParams(
                login: login,
                password: password);
            var loginRequest = apiConfig.prepareWebRequest(APIMethod.login(), loginRequestData);
            yield return loginRequest.SendWebRequest();

            if(loginRequest.isNetworkError || loginRequest.isHttpError)
            {
                Debug.Log("Login request error: " + loginRequest.error);
                yield break;
            }

            Debug.Log("Form upload complete!");
            var loginResponse = ParseResponse<LoginResponse>(loginRequest);
            var user = new UserId(loginResponse.data.user);
            var authToken = new AuthToken(loginResponse.token);
            authData.UpdateWithLoginInformation(
                user: user,
                isAdmin: loginResponse.data.is_admin,
                authToken: authToken);

            var profileRequest = apiConfig.prepareWebRequestWithAuthorization(APIMethod.profile(user), authToken, null);
            yield return profileRequest.SendWebRequest();

            if (profileRequest.isNetworkError || profileRequest.isHttpError)
            {
                Debug.Log("Profile request error: " + profileRequest.error);
                yield break;
            }

            var profileResponse = ParseResponse<ProfileResponse>(profileRequest);
            var attributes = profileResponse.attributes.Select(a => a.ToAttribute()).ToArray();
            var items = profileResponse.items.Select(i => i.ToItem()).ToArray();
            var nativeAbilities = profileResponse.native_abilities.Select(a => a.ToAbility()).ToArray();
            profileData.UpdateWithProfileInformation(
                credits: profileResponse.credits.amount,
                attributes: attributes,
                nativeAbilities: nativeAbilities,
                items: items
            );
        }

        private T ParseResponse<T>(UnityWebRequest request)
        {
            var data = request.downloadHandler.text;
            var response = JsonUtility.FromJson<T>(data);
            return response;
        }
    }
}

[Serializable]
[SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
internal class LoginParams
{
    public string login;
    public string password;

    public LoginParams(string login, string password)
    {
        this.login = login;
        this.password = password;
    }
}

// disable 'never assigned' warning as these structures are just for JSON serialization
#pragma warning disable 649
[Serializable]
internal class LoginResponse
{
    public string token;
    public LoginResponseData data;
}

[Serializable]
[SuppressMessage("ReSharper", "InconsistentNaming")]
internal class LoginResponseData
{
    public long user;
    public bool is_admin;
}

[Serializable]
internal class CreditsResponse
{
    public int amount;
}

[Serializable]
internal class KindData
{
    public int id;
    public string name;
}


[Serializable]
internal class ProvidedAbility : KindData
{
    public Ability ToAbility()
    {
        return new Ability(name: name, isNative: false);
    }
}

[Serializable]
[SuppressMessage("ReSharper", "InconsistentNaming")]
internal class ItemKindData : KindData
{
    public ProvidedAbility[] provided_abilities;
}

[Serializable]
internal class AttributeData
{
    public KindData kind;
    public int value;

    public Attribute ToAttribute()
    {
        return new Attribute(name: kind.name, value: value);
    }
}

[Serializable]
internal class NativeAbilityData
{
    public int id;
    public KindData kind;

    public Ability ToAbility()
    {
        return new Ability(name: kind.name, isNative: true);
    }
}

[Serializable]
internal class ItemData
{
    public int id;
    public ItemKindData kind;

    public Item ToItem()
    {
        return new Item(
            name: kind.name,
            providedAbilities: kind.provided_abilities.Select(a => a.ToAbility()).ToArray());
    }
}

[Serializable]
[SuppressMessage("ReSharper", "InconsistentNaming")]
internal class ProfileResponse
{
    public long id;
    public string visible_name;
    public bool is_admin;
    public CreditsResponse credits;

    public AttributeData[] attributes;
    public NativeAbilityData[] native_abilities;
    public ItemData[] items;
}
#pragma warning restore 649
