﻿using JetBrains.Annotations;
using UnityEngine;

namespace Ilych.Cyberpunk0.Util
{
  public class AnimatorBoolSetter : MonoBehaviour
  {
    private Animator animator;

    [PublicAPI]
    public void SetTrue(string name)
    {
      var animator = GetAnimator();
      animator.SetBool(name, true);
    }

    [PublicAPI]
    public void SetFalse(string name)
    {
      var animator = GetAnimator();
      animator.SetBool(name, false);
    }

    private Animator GetAnimator()
    {
      if (animator == null)
      {
        animator = GetComponent<Animator>();
      }
      return animator;
    }
  }
}
